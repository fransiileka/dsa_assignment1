import ballerina/grpc;
import ballerina/protobuf;
import ballerina/protobuf.types.wrappers;

const string BUFFER_DESC = "0A0C6275666665722E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F2292010A045573657212160A067573657249641801200128095206757365724964121A0A086C6173746E616D6518022001280952086C6173746E616D65121C0A0966697273746E616D65180320012809520966697273746E616D6512140A05656D61696C1804200128095205656D61696C12220A0770726F66696C6518052001280B32082E50726F66696C65520770726F66696C6522650A0750726F66696C65121A0A08757365726E616D651801200128095208757365726E616D65121B0A09757365725F747970651802200128095208757365725479706512210A07636F757273657318032003280B32072E436F757273655207636F757273657322D5010A06436F75727365121F0A0B636F757273655F636F6465180120012809520A636F75727365436F6465121F0A0B636F757273655F6E616D65180220012809520A636F757273654E616D65122D0A12636F757273655F6465736372697074696F6E1803200128095211636F757273654465736372697074696F6E122B0A11636F757273655F696E7374727563746F721804200128095210636F75727365496E7374727563746F72122D0A0B61737369676E6D656E747318052003280B320B2E41737369676E6D656E74520B61737369676E6D656E747322E2010A0A41737369676E6D656E7412140A05656D61696C1801200128095205656D61696C121F0A0B636F757273655F636F6465180220012809520A636F75727365436F646512270A0F61737369676E6D656E745F6E616D65180320012809520E61737369676E6D656E744E616D65122E0A1361737369676E6D656E745F6475655F64617465180420012809521161737369676E6D656E744475654461746512140A056D61726B7318052001280552056D61726B7312160A06776569676874180620012805520677656967687412160A066D61726B656418072001280852066D61726B656422AD010A054D61726B7312270A0F61737369676E6D656E745F6E616D65180120012809520E61737369676E6D656E744E616D6512290A1061737369676E6D656E745F6D61726B73180220012805520F61737369676E6D656E744D61726B73122B0A1161737369676E6D656E745F636F75727365180320012809521061737369676E6D656E74436F7572736512230A0D6C6561726E65725F656D61696C180420012809520C6C6561726E6572456D61696C22430A08526567697374657212160A067573657249641801200128095206757365724964121F0A0B636F757273655F636F6465180220012809520A636F75727365436F6465224E0A1341737369676E436F757273655265717565737412160A067573657249641801200128095206757365724964121F0A0B636F757273655F636F6465180220012809520A636F75727365436F646522370A14437265617465436F75727365526573706F6E7365121F0A0B636F757273655F636F6465180120012809520A636F75727365436F646522470A1853696E676C6541737369676E6D656E74526573706F6E7365122B0A0A61737369676E6D656E7418022001280B320B2E41737369676E6D656E74520A61737369676E6D656E7422450A1453696E676C65436F75727365526573706F6E7365122D0A0D63757272656E74436F7572736518022001280B32072E436F75727365520D63757272656E74436F7572736532BF030A0662756666657212340A0E6372656174655F636F757273657312072E436F757273651A152E437265617465436F75727365526573706F6E73652801300112440A0E61737369676E5F636F757273657312142E41737369676E436F75727365526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512350A0C6372656174655F757365727312052E557365721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112410A127375626D69745F61737369676E6D656E7473120B2E41737369676E6D656E741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112500A13726571756573745F61737369676E6D656E7473121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A192E53696E676C6541737369676E6D656E74526573706F6E7365300112360A0C7375626D69745F6D61726B7312062E4D61726B731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112350A08726567697374657212092E52656769737465721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801620670726F746F33";

public isolated client class bufferClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, BUFFER_DESC);
    }

    isolated remote function assign_courses(AssignCourseRequest|ContextAssignCourseRequest req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        AssignCourseRequest message;
        if req is ContextAssignCourseRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("buffer/assign_courses", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function assign_coursesContext(AssignCourseRequest|ContextAssignCourseRequest req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        AssignCourseRequest message;
        if req is ContextAssignCourseRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("buffer/assign_courses", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function create_users() returns Create_usersStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("buffer/create_users");
        return new Create_usersStreamingClient(sClient);
    }

    isolated remote function submit_assignments() returns Submit_assignmentsStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("buffer/submit_assignments");
        return new Submit_assignmentsStreamingClient(sClient);
    }

    isolated remote function submit_marks() returns Submit_marksStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("buffer/submit_marks");
        return new Submit_marksStreamingClient(sClient);
    }

    isolated remote function register() returns RegisterStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("buffer/register");
        return new RegisterStreamingClient(sClient);
    }

    isolated remote function request_assignments(string|wrappers:ContextString req) returns stream<SingleAssignmentResponse, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("buffer/request_assignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        SingleAssignmentResponseStream outputStream = new SingleAssignmentResponseStream(result);
        return new stream<SingleAssignmentResponse, grpc:Error?>(outputStream);
    }

    isolated remote function request_assignmentsContext(string|wrappers:ContextString req) returns ContextSingleAssignmentResponseStream|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("buffer/request_assignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        SingleAssignmentResponseStream outputStream = new SingleAssignmentResponseStream(result);
        return {content: new stream<SingleAssignmentResponse, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function create_courses() returns Create_coursesStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("buffer/create_courses");
        return new Create_coursesStreamingClient(sClient);
    }
}

public client class Create_usersStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendUser(User message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextUser(ContextUser message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_assignmentsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendAssignment(Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextAssignment(ContextAssignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_marksStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendMarks(Marks message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextMarks(ContextMarks message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class RegisterStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendRegister(Register message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextRegister(ContextRegister message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class SingleAssignmentResponseStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|SingleAssignmentResponse value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|SingleAssignmentResponse value;|} nextRecord = {value: <SingleAssignmentResponse>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Create_coursesStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse(Course message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse(ContextCourse message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveCreateCourseResponse() returns CreateCourseResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <CreateCourseResponse>payload;
        }
    }

    isolated remote function receiveContextCreateCourseResponse() returns ContextCreateCourseResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <CreateCourseResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class BufferSingleAssignmentResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendSingleAssignmentResponse(SingleAssignmentResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextSingleAssignmentResponse(ContextSingleAssignmentResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class BufferCreateCourseResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCreateCourseResponse(CreateCourseResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCreateCourseResponse(ContextCreateCourseResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class BufferStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(wrappers:ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextAssignmentStream record {|
    stream<Assignment, error?> content;
    map<string|string[]> headers;
|};

public type ContextUserStream record {|
    stream<User, error?> content;
    map<string|string[]> headers;
|};

public type ContextRegisterStream record {|
    stream<Register, error?> content;
    map<string|string[]> headers;
|};

public type ContextSingleAssignmentResponseStream record {|
    stream<SingleAssignmentResponse, error?> content;
    map<string|string[]> headers;
|};

public type ContextCreateCourseResponseStream record {|
    stream<CreateCourseResponse, error?> content;
    map<string|string[]> headers;
|};

public type ContextCourseStream record {|
    stream<Course, error?> content;
    map<string|string[]> headers;
|};

public type ContextMarksStream record {|
    stream<Marks, error?> content;
    map<string|string[]> headers;
|};

public type ContextAssignment record {|
    Assignment content;
    map<string|string[]> headers;
|};

public type ContextUser record {|
    User content;
    map<string|string[]> headers;
|};

public type ContextRegister record {|
    Register content;
    map<string|string[]> headers;
|};

public type ContextSingleAssignmentResponse record {|
    SingleAssignmentResponse content;
    map<string|string[]> headers;
|};

public type ContextCreateCourseResponse record {|
    CreateCourseResponse content;
    map<string|string[]> headers;
|};

public type ContextCourse record {|
    Course content;
    map<string|string[]> headers;
|};

public type ContextMarks record {|
    Marks content;
    map<string|string[]> headers;
|};

public type ContextAssignCourseRequest record {|
    AssignCourseRequest content;
    map<string|string[]> headers;
|};

@protobuf:Descriptor {value: BUFFER_DESC}
public type Assignment record {|
    string email = "";
    string course_code = "";
    string assignment_name = "";
    string assignment_due_date = "";
    int marks = 0;
    int weight = 0;
    boolean marked = false;
|};

@protobuf:Descriptor {value: BUFFER_DESC}
public type User record {|
    string userId = "";
    string lastname = "";
    string firstname = "";
    string email = "";
    Profile profile = {};
|};

@protobuf:Descriptor {value: BUFFER_DESC}
public type Register record {|
    string userId = "";
    string course_code = "";
|};

@protobuf:Descriptor {value: BUFFER_DESC}
public type SingleAssignmentResponse record {|
    Assignment assignment = {};
|};

@protobuf:Descriptor {value: BUFFER_DESC}
public type CreateCourseResponse record {|
    string course_code = "";
|};

@protobuf:Descriptor {value: BUFFER_DESC}
public type SingleCourseResponse record {|
    Course currentCourse = {};
|};

@protobuf:Descriptor {value: BUFFER_DESC}
public type Course record {|
    string course_code = "";
    string course_name = "";
    string course_description = "";
    string course_instructor = "";
    Assignment[] assignments = [];
|};

@protobuf:Descriptor {value: BUFFER_DESC}
public type Marks record {|
    string assignment_name = "";
    int assignment_marks = 0;
    string assignment_course = "";
    string learner_email = "";
|};

@protobuf:Descriptor {value: BUFFER_DESC}
public type Profile record {|
    string username = "";
    string user_type = "";
    Course[] courses = [];
|};

@protobuf:Descriptor {value: BUFFER_DESC}
public type AssignCourseRequest record {|
    string userId = "";
    string course_code = "";
|};

