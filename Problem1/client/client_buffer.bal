import ballerina/io;

bufferClient all = check new ("http://localhost:9090");

public function main() returns error? {


    ///////////////////////////////////Datasets///////////////////////////////////////////////////////////////////////
    //declering Users
    User[] newUsers = [
        {userId:"1", firstname:"Frans", lastname:"Ileka", email:"fileka@gmail.com", profile:{username:"zeca", user_type:"learner", courses:[]}},
        {userId:"2", firstname:"Petrus", lastname:"Uugwanga", email:"puugwangax@gmail.com", profile:{username:"top", user_type:"learner", courses:[]}},
        {userId:"3", firstname:"Helvi", lastname:"Lesheni", email:"hlesheni@gmail.com", profile:{username:"paris", user_type:"adminstrator", courses:[]}},
        {userId:"4", firstname:"Jean", lastname:"Nico", email:"jnico@gmail.com", profile:{username:"jeany", user_type:"adminstrator", courses:[]}},
        {userId:"5", firstname:"Salty", lastname:"Kanko", email:"skanko@gmail.com", profile:{username:"skay", user_type:"assessor", courses:[]}},
        {userId:"6", firstname:"Simon", lastname:"Mikka", email:"smikka@gmail.com", profile:{username:"sal", user_type:"assessor", courses:[]}},
        {userId:"7", firstname:"Watya", lastname:"Kapoky", email:"wkapoky@gmail.com", profile:{username:"wat", user_type:"learner", courses:[]}},
        {userId:"8", firstname:"Kandeshi", lastname:"Shivute", email:"ybs@gmail.com", profile:{username:"yb", user_type:"learner", courses:[]}}
    ];

   //declering the course
    Course[] courses = [
        {course_code:"NPG60S1", course_name:"Network Programming", course_description:"Introduces the network communication using sockets", course_instructor:"", assignments:[]},
        {course_code:"ITC60S1", course_name:"Internet Computing", course_description:"Introduces the socket Programming ", course_instructor:"", assignments:[]},
        {course_code:"CNT60S1", course_name:"cloud networking", course_description:"Introduces the cloud computing theory", course_instructor:"", assignments:[]},
        {course_code:"DSA60S1", course_name:"Distibution", course_description:"Introduces more on Network Programmming", course_instructor:"", assignments:[]},
        {course_code:"WLT60S1", course_name:"Wireless Technologies", course_description:"This course introduces Wireless communication", course_instructor:"", assignments:[]},
        {course_code:"ADS60S1", course_name:"Advanced Networking", course_description:"This course introduces Cryptograph in computing", course_instructor:"", assignments:[]},
        {course_code:"CNE60S1", course_name:"Core network Engineering", course_description:"This course introduces the set up of the network topologies", course_instructor:"", assignments:[]},
        {course_code:"CMN60S1", course_name:"Communication Networks", course_description:"This course introduces more on the implementation of IP addrressing", course_instructor:"", assignments:[]}
    ];
    
    
    //declering the assignment
    Assignment[] assignments = [
        {email:"fileka@gmail.com", course_code:"NPG60S1", assignment_name:"Assignment 1", assignment_due_date:"2022-01-08", marks:0, weight:70, marked:false},
        {email:"", course_code:"NPG60S1", assignment_name:"Assignment 2", assignment_due_date:"2022-01-09",  marks:0, weight:70, marked:false},
        {email:"", course_code:"ITC60S1", assignment_name:"Assignment 3", assignment_due_date:"2022-01-10",  marks:0, weight:70, marked:false},
        {email:"", course_code:"CNT60S1", assignment_name:"Assignment 5", assignment_due_date:"2022-01-11",  marks:0, weight:70, marked:false},
        {email:"", course_code:"DSA60S1", assignment_name:"Assignment 6", assignment_due_date:"2022-01-12",  marks:0, weight:70, marked:false},
        {email:"", course_code:"WLT60S1", assignment_name:"Assignment 7", assignment_due_date:"2022-01-13",  marks:0, weight:70, marked:false},
        {email:"", course_code:"ADS60S1", assignment_name:"Assignment 8", assignment_due_date:"2022-01-15",  marks:0, weight:70, marked:false},
        {email:"", course_code:"CNE60S1", assignment_name:"Assignment 9", assignment_due_date:"2022-01-16",  marks:0, weight:70, marked:false},
        {email:"", course_code:"CMN60S1", assignment_name:"Assignment 10", assignment_due_date:"2022-01-17",  marks:0, weight:70, marked:false}
    ];
    //declering marks
    Marks[] marks = [
        {assignment_name:"Assignment 1", assignment_marks:54, assignment_course:"NPG60S1", learner_email:"fileka@gmail.com"},
        {assignment_name:"Assignment 2", assignment_marks:57, assignment_course:"NPG60S1", learner_email:"fileka@gmail.com"},
        {assignment_name:"Assignment 3", assignment_marks:26, assignment_course:"ITC60S1", learner_email:"fileka@gmail.com"},
        {assignment_name:"Assignment 4", assignment_marks:30, assignment_course:"CNT60S1", learner_email:"fileka@gmail.com"},
        {assignment_name:"Assignment 5", assignment_marks:60, assignment_course:"DSA60S1", learner_email:"fileka@gmail.com"},
        {assignment_name:"Assignment 6", assignment_marks:17, assignment_course:"WLT60S1", learner_email:"ybs@gmail.com"}
    ];
     //decleraring the course registry
    Register[] informations = [
        {userId:"1", course_code:"NPG60S1"},
        {userId:"1", course_code:"ITC60S1"},
        {userId:"1", course_code:"CNT60S1"},
        {userId:"1", course_code:"DSA60S1"},
        {userId:"1", course_code:"WLT60S1"}
    ];


    //argumentative Operations 
    //Creating users
    io:println("                                              ");
    io:println("Creatin user CLIENT streaming rpc in progress");
    io:println("______________________________________________");
    Create_usersStreamingClient create_usersStreamingClient = check all->create_users();

    foreach User create_usersRequest in newUsers{
        check create_usersStreamingClient->sendUser(create_usersRequest);
    }
    check create_usersStreamingClient->complete();
    string? create_usersResponse = check create_usersStreamingClient->receiveString();
    io:println(create_usersResponse);

    //Creating courses
    io:println("                                              ");
    io:println("Creating courses BI-DIRECTIONAL streaming rpc in progress"); 
    io:println("______________________________________________");  
    Create_coursesStreamingClient create_coursesStreamingClient = check all->create_courses();
    future<error?> future1 = start fetchResponse(create_coursesStreamingClient);

    foreach Course create_coursesRequest in courses{
        check create_coursesStreamingClient->sendCourse(create_coursesRequest);
    }
    check create_coursesStreamingClient->complete();
    CreateCourseResponse? create_coursesResponse = check create_coursesStreamingClient->receiveCreateCourseResponse();
    io:println(create_coursesResponse);

    error? unionResult = wait future1;
    unionResult = check unionResult;

    //Assigning courses to the assessor
    AssignCourseRequest assign_coursesRequest = {userId: "15", course_code: "CNE60S1"};
    string assign_coursesResponse = check all->assign_courses(assign_coursesRequest);
    io:println(assign_coursesResponse);
    
    //For student to register to the course
    io:println("                                              ");    
    io:println("Register CLIENT streaming rpc in progress");
    io:println("______________________________________________");
    RegisterStreamingClient registerStreamingClient = check all->register();
    foreach Register registerRequest in informations{
            check registerStreamingClient->sendRegister(registerRequest);
    }
    check registerStreamingClient->complete();
    string? registerResponse = check registerStreamingClient->receiveString();
    io:println(registerResponse);
    
    //Assignment Submission
    io:println("                                              ");
    io:println("Making  a submit assingment CLIENT streaming rpc");
    io:println("______________________________________________");
    Submit_assignmentsStreamingClient submit_assignmentsStreamingClient = check all->submit_assignments();
    foreach Assignment submit_assignmentsRequest in assignments{
        check submit_assignmentsStreamingClient->sendAssignment(submit_assignmentsRequest);
    }
    check submit_assignmentsStreamingClient->complete();
    string? submit_assignmentsResponse = check submit_assignmentsStreamingClient->receiveString();
    io:println(submit_assignmentsResponse);

    //Marks Submission 
    io:println("                                              ");
    io:println("Making  a submit marks CLIENT streaming rpc");
    io:println("______________________________________________");
    Submit_marksStreamingClient submit_marksStreamingClient = check all->submit_marks();
    foreach Marks submit_marksRequest in marks{
        check submit_marksStreamingClient->sendMarks(submit_marksRequest);
    }
    check submit_marksStreamingClient->complete();
    string? submit_marksResponse = check submit_marksStreamingClient->receiveString();
    io:println(submit_marksResponse);
    
    //Requesting an unmarked assignment
    io:println("                                              ");
    io:println("Making  a request assignemnt SERVER streaming rpc");
    io:println("______________________________________________");
    string request_assignmentsRequest = "NPG60S1";
    stream<SingleAssignmentResponse, error?> request_assignmentsResponse = check all->request_assignments(request_assignmentsRequest);
    check request_assignmentsResponse.forEach(function(SingleAssignmentResponse value) {
        io:println(value);
    });

}

//Function to fetch the create courses response from the server
function fetchResponse(Create_coursesStreamingClient create_course_stream) returns error?{
   CreateCourseResponse? response = check create_course_stream->receiveCreateCourseResponse();

    while !(response is ()){
            
        io:println("                                              ");
        io:println("The course got recieved successfully:", response);
        response = check create_course_stream->receiveCreateCourseResponse();
    }
}
