import ballerina/graphql;

public type Statistics record {|
    string date;
    string region;
    int deaths;
    int confirmed_cases;
    int recoveries;
    int tested;
|};

service /open_graphql on new graphql:Listener(9090) {
    private Statistics statistics;

    function init() {
        self.statistics = {date: "18/09/2022", region: "Khomas", deaths: 39, confirmed_cases: 465, recoveries: 67, tested: 1200};
    }

    resource function get statistics() returns Statistics {
        return self.statistics;
    }

    remote function updateCovidStats(string date, string region, int deaths, int confirmed_cases, int recoveries, int tested) returns Statistics {
        self.statistics.date = date;
        self.statistics.region = region;
        self.statistics.deaths = deaths;
        self.statistics.confirmed_cases = confirmed_cases;
        self.statistics.recoveries = recoveries;
        self.statistics.tested = tested;
        return self.statistics;
    }
}
